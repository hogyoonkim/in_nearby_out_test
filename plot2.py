from common import get_all, get_jaccard, get_euclidean, get_z_axis, get_euclid2_coef
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import json


# init data
euclid_limit = 6.8
jaccard_limit = 0.26
monitor = []

# init graph
fig = plt.figure(1)
#ax = fig.add_subplot(111)
ax = fig.add_subplot(111, projection='3d')
print('jccard, euclid, z_axis, big_diff, none_big_diff, y')

# search record
record_cnt = len(get_all())
idx = 0
progress = 1
for r in get_all():
    idx += 1
    if idx == int(record_cnt*progress/10):
        #print('Loading... ', progress*10, '%')
        progress += 1

    target_spot = json.loads(r['target_spot'])
    compare_spot = json.loads(r['compare_spot'])
    compare_dict = dict()
    for c in compare_spot['wlanDatas']:
        compare_dict[c['bssid']] = c

    id = r['id']
    name = r['name']
    jaccard = get_jaccard(target_spot, compare_spot)
    #euclid = get_euclidean(target_spot, compare_dict)
    euclid = get_euclid2_coef(target_spot, compare_dict, 0)
    z_axis, big_diff, none_big_diff = get_z_axis(target_spot, compare_spot, compare_dict)
    state = r['state']

    if state == 'nearby':
        state = 'other'

    if state == 'in':
        symbol = 'b*'
    elif state == 'nearby':
        symbol = 'y^'
    elif state == 'out':
        symbol = 'rs'
    else:
        symbol = 'go'

    #ax.plot([jaccard], [euclid], symbol)
    ax.plot([jaccard], [euclid], [z_axis], symbol)
    print('%s, %s, %s, %s, %s, %s' % (jaccard, euclid, z_axis, big_diff, none_big_diff, 1 if state == 'in' else 0))

    if jaccard >= 0.33 and jaccard <= 0.42 and euclid >= 4.6 and euclid <= 6.2:
        monitor.append((r['id'], r['name'], state, euclid, jaccard))

ax.set_xlabel('jaccard')
ax.set_xlim(0, 1)
ax.set_ylabel('euclidean')
ax.set_ylim(0, 30)
ax.set_zlabel('z_axis')
ax.set_zlim(0, 10)

# for m in monitor:
#     print(m)

plt.show()
