import json
from base.database import get_read_conn, get_write_conn
import other_conv

def get_spot_from_id(spot):
    conn = get_read_conn()
    with conn.cursor() as cursor:
        cursor.execute('''select frequency, ssid, bssid, level from wlan_scan where spot = %s and disabled = %s''', (spot, 0))
        rows = cursor.fetchall()
    conn.close()
    key_seq = 'frequency', 'ssid', 'bssid', 'level'
    return [dict(zip(key_seq, row)) for row in rows]


def insert_into_ino_test(name, target, compare, state):
    conn = get_write_conn()
    with conn.cursor() as cursor:
        cursor.execute('''insert into in_nearby_out_test(name, target_spot, compare_spot, state) 
        values(%s, %s, %s, %s)''', (name, target, compare, state))
        conn.commit()
    conn.close()


def select_ino_other(t):
    conn = get_write_conn()
    with conn.cursor() as cursor:
        cursor.execute(""" select id from spot
        where id in (select spot from wlan_scan where bssid in (select bssid from wlan_scan where spot = %s
        and bssid != '00:00:00:00:00:00' and frequency < 4000 
        and ssid not like '%%Egg%%' 
        and ssid not like '%%T Pocket%%' 
        and ssid not like '%%AndroidHotspot%%' 
        and ssid not like '%%AndroidAP%%' 
        and ssid not like '%%iPhone%%'
        and ssid not like '%%phone%%' 
        and ssid not like '%%iPad%%') group by spot)
        and disabled = false and id not like %s
        and name not like '%%인%%' and name not like '%%니어%%' and id >= 700000""", (t, t))
        rows = cursor.fetchall()
    conn.close()
    key_seq = 'id'
    return [dict(zip(key_seq, row)) for row in rows]


spot = input("target의 spot 번호를 입력해 주세요. \n")
num = spot
name = input("target의 name 을 입력해 주세요. \n")
target = json.dumps(dict(ans=[spot], wlanDatas=get_spot_from_id(spot)))

print('=========================================')

a = True
while a:
    spot = input("\n 비교할 대상의 spot 번호를 입력해 주세요."
                 "\n other 를 넣으려면 other를 입력해 주세요."
                 "\n 끝이면 end를 입력해주세요. \n")
    if spot != 'end' and ',' not in spot and spot != 'other':
        state = input("in / nearby / out / other 를 입력해 주세요. \n")
        compare = json.dumps(dict(ans=[spot], wlanDatas=get_spot_from_id(spot)))
        insert_into_ino_test(name, target, compare, state)

    elif spot != 'end' and spot == 'other':
        state = 'other'

        print(num)
        compare = select_ino_other(num)
        for i in range(0, len(select_ino_other(num))):
            other_num = json.dumps(dict(ans=[compare[i]['i']], wlanDatas=get_spot_from_id(compare[i]['i'])))
            insert_into_ino_test(name, target, other_num, state)

        #=============================================
        # spot = other_conv.other_conv()
        #
        # for i in spot.split(',')[:-1]:
        #     print(i)
        #     compare = json.dumps(dict(ans=[i], wlanDatas=get_spot_from_id(i)))
        #     insert_into_ino_test(name, target, compare, state)
        # ============================================

        a = False
        print('입력 끝.')
    else:
        a = False
