from base.database import get_read_conn
from math import sqrt


def get_all():
    conn = get_read_conn()
    with conn.cursor() as cursor:
        cursor.execute('''select * from in_nearby_out_test where dis = 0''')
        rows = cursor.fetchall()
    conn.close()
    key_seq = 'id', 'name', 'target_spot', 'compare_spot', 'state', 'dis'
    return [dict(zip(key_seq, row)) for row in rows]


def get_euclid_coef(target_spot, compare_dict, delta, debug=False):
    sum = 0
    b = 0
    max_cnt = 30
    diff = []

    for t in target_spot['wlanDatas']:
        if t['bssid'] in compare_dict:
            c = compare_dict[t['bssid']]
            if t['frequency'] < 4000 and c['frequency'] < 4000:
                target_level = t['level']
                compare_level = c['level']
                target_level += delta

                target_level = -55 if target_level >= -55 else target_level
                compare_level = -55 if compare_level >= -55 else compare_level

                diff.append(pow(compare_level - target_level, 2))
                b += 1

    b = min(b, max_cnt)
    for s in sorted(diff, reverse=True)[:max_cnt]:
        if debug:
            print(sum, s)
        sum += s

    if b == 0:
        ans = 20
    else:
        ans = (sum / b) ** (1 / 2)

    if debug:
        print('ans : ', ans)
    return ans


def get_euclid2_coef(target_spot, compare_dict, delta, debug=False):
    sum = 0
    b = 0
    max_cnt = 7
    diff = []

    for t in target_spot['wlanDatas']:
        if t['bssid'] in compare_dict:
            c = compare_dict[t['bssid']]
            if t['frequency'] < 4000 and c['frequency'] < 4000:
                target_level = t['level']
                compare_level = c['level']
                target_level += delta
                target_level = -50 if target_level >= -50 else target_level
                compare_level = -50 if compare_level >= -50 else compare_level

                diff.append(pow(compare_level - target_level, 2))
                b += 1

    b = min(b, max_cnt)
    for s in sorted(diff, reverse=True)[:max_cnt]:
        if debug:
            print(sum, s)
        sum += s

    if b == 0:
        ans = 30
    else:
        ans = (sum / b) ** (1 / 2)

    if debug:
        print('ans : ', ans)
    return ans


def parametric_search(target_spot, compare_dict, debug=False):
    min = -20
    max = 20
    mid = 0

    while min < max:
        mid = (min + max) / 2
        a = get_euclid_coef(target_spot, compare_dict, min, debug)
        b = get_euclid_coef(target_spot, compare_dict, max, debug)
        if abs(a) <= abs(b):
            max = mid - 0.00000001
        else:
            min = mid + 0.00000001
    return mid


def get_jaccard(target_spot, compare_spot):
    target_dict = dict()
    for t in target_spot['wlanDatas']:
        target_dict[t['bssid']] = t

    uni = 0
    inter = 0
    for c in compare_spot['wlanDatas']:
        uni += 1
        if c['bssid'] in target_dict:
            t = target_dict[c['bssid']]
            if t['frequency'] < 4000 and c['frequency'] < 4000:
                inter += 1

    a = set(c['bssid'] for c in target_spot['wlanDatas'] if c['frequency'] < 4000)
    b = set(c['bssid'] for c in compare_spot['wlanDatas'] if c['frequency'] < 4000)

    return inter / len(a | b)


def get_euclidean(target_spot, compare_dict, debug=False):
    delta = parametric_search(target_spot, compare_dict, False)
    if debug:
        print('delta : ', delta)
    return get_euclid_coef(target_spot, compare_dict, delta, debug)


def get_z_axis(target_spot, compare_spot, compare_dict, debug=False):
    delta = parametric_search(target_spot, compare_dict, False)

    inter = 0
    big_diff = 0
    none_big_diff = 0

    for t in target_spot['wlanDatas']:
        for c in compare_spot['wlanDatas']:
            if c['bssid'] == t['bssid']:
                if t['level'] >= -70 and c['level'] + delta >= -70:
                    inter += 1

                if (t['level'] >= -90 or c['level'] >= -90) and abs(t['level'] - (c['level'] + delta)) > 25:
                    big_diff += 1

    for c in compare_spot['wlanDatas']:
        if c not in target_spot['wlanDatas'] and c['level'] >= -75:
            none_big_diff += 1

    return inter, big_diff, none_big_diff

    # if inter >= 10:
    #     if none_big_diff >= 10:
    #         return 5
    #     elif big_diff >= 1:
    #         return 0
    #     else:
    #         return 10
    # else:
    #     return inter
