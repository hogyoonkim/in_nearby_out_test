import pymysql

from base.config import READ_DATABASE, WRITE_DATABASE, SPOT_READ_DATABASE


def get_read_conn():
    return pymysql.connect(**READ_DATABASE)

def get_write_conn():
    return pymysql.connect(**WRITE_DATABASE)

def get_spot_read_conn():
    return pymysql.connect(**SPOT_READ_DATABASE)
